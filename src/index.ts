import express from 'express';
import { handleSearch } from './api/search';
import { handleContent } from './api/content'
import cors from 'cors';

const port = 3001
const server = express();

// Only for testing purpose, remove from prodution deployment!
server.use(cors());

// API Route bidings
server.get('/search', handleSearch.bind(this));
server.get('/content', handleContent.bind(this));

// Running the host listener
server.listen(port, () => console.log('\x1b[33m%s\x1b[0m', `Stealing from the rich and giving to the poor, on port ${port}`));
