/**
 * API Search Response handler
 */

import { success, error } from './responseModels';
import { TMDBService, SearchResult, Movie } from '../services';
import { take } from 'rxjs/operators';

const apiKey = '56df08b73af0e1443337dfb9d09da7a5';
const api: TMDBService = new TMDBService(apiKey);

export const handleSearch = (request, response) => {

    const keyword = request?.query?.keyword;

    api.search.movies(keyword).pipe(take(1)).subscribe((movies: SearchResult<Movie>) => {

        const results = movies?.results ?? ['api request error'];
        results ? response.status(200).json(success('OK', results, response.statusCode))
            : response.status(500).json(error('Something went wrong', response.statusCode));

    });
};