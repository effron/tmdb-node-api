/**
 * @desc Our very nice API Model response handler 
 * @author Ivan Herczeg
 */

/**
 * @desc    Response model on SUCCESS
 * @param   {string} message
 * @param   {object | array} results
 * @param   {number} statusCode
 */
 export const success = (message, results, statusCode) => {
    return {
      message,
      error: false,
      code: statusCode,
      results
    };
  };
  
  /**
   * @desc    Response model on ERROR
   * @param   {string} message
   * @param   {number} statusCode
   */
  export const error = (message, statusCode) => {

    // List of common HTTP request code
    const codes = [200, 201, 400, 401, 404, 403, 422, 500];
  
    // Get matched code
    const findCode = codes.find((code) => code == statusCode);
  
    statusCode = findCode ?? 500;
  
    return {
      message,
      code: statusCode,
      error: true
    };
  };
  