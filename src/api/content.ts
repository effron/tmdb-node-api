/**
 * API Search Response handler
 */
import { success, error } from './responseModels';
import { TMDBService, SearchResult, Movie } from '../services';
import { take } from 'rxjs/operators';

const apiKey = '56df08b73af0e1443337dfb9d09da7a5';
const api: TMDBService = new TMDBService(apiKey);

export const handleContent = (request, response) => {
    const id = request?.query?.id;
    api.content.details(id).pipe(take(1)).subscribe((movie: Movie) => {
        const result = movie ?? ['service request error'];
        result ? response.status(200).json(success('OK', result, response.statusCode))
            : response.status(500).json(error("Something went wrong", response.statusCode));

    });
};