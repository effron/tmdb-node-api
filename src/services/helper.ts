import { Observable, from } from 'rxjs';
import { map } from 'rxjs/operators';
import axios from 'axios';
import { ajax } from 'rxjs/ajax';

export class Helper {
	/**
	 * @param url 
	 */
	public static ajaxObservable(url): Observable<any> {
		return from(axios.get(url)).pipe(
			map(response => {
				return response.data;
			})
		)

		return ajax({
			url: url,
			crossDomain: true
		}).pipe(
			map(ajax => {
				return ajax.response;
			})
		)
	}
}