import { Observable } from 'rxjs';

import { Api } from '../model/api';
import {
    SearchResult,
    Movie,
    Company,
    Collection,
    Keyword,
} from '../';
import { Helper } from '../helper';

export class SearchService {

	constructor(private api: Api) {
	}

	/**
	 * Builds the endpoint URL depending on which resource we are looking for
	 * @param resource company, collection, keyword, movie, tv, person...
	 * @param query
	 * @param page
	 * @returns {string}
	 */
	private url(resource: string, query: string, page: number) {
		return `${this.api.baseUrl}/search/${resource}?api_key=${this.api.apiKey}&language=${this.api.language}&query=${query}&page=${page}`;
	}

	keywords(query: string, page = 1): Observable<SearchResult<Keyword>> {
		return Helper.ajaxObservable(this.url('keyword', query, page));
	}

	movies(query: string, page = 1): Observable<SearchResult<Movie>> {
		return Helper.ajaxObservable(this.url('movie', query, page));
	}

}