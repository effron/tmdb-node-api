import { Observable } from 'rxjs';


import { Api } from '../model/api';
import { Helper } from '../helper';
import { MovieDetails } from '..';

export class ContentService {

	constructor(private api: Api) {
	}

	details(movieId: number): Observable<MovieDetails> {
		const url = `${this.api.baseUrl}/movie/${movieId}?api_key=${this.api.apiKey}&language=${this.api.language}`;
		return Helper.ajaxObservable(url);
	}

}