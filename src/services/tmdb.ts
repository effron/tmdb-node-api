import { Api } from './model/api';
import { SearchService } from './features/search';
import { ContentService } from './features/content';

export class TMDBService {

	api: Api;

	private _search: SearchService;
	private _content: ContentService;

	constructor(apiKey: string, language = 'en') {
		this.api = new Api();
		this.api.apiKey = apiKey;
		this.api.language = language;
	}

	get search() {
		return this._search || (this._search = new SearchService(this.api));
	}

	get content() {
		return this._content || (this._content = new ContentService(this.api));
	}

}