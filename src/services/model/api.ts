export class Api {
	apiKey: string;
	language: string = 'en';
	baseUrl = 'https://api.themoviedb.org/3';
}