NodeJS API implementation of the TMDB

### `npm run compile`

It compiles the application. You need typescript.

### `npm run serve`

Runs the compiled server.\

- Ask me if it's not resonating with you: ivan.herczeg@mail.com